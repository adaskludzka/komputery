import java.io.Serializable;

public class User implements Serializable {

    private String name;
    private String surName;
    private String mail;

    public User(String name, String surName, String mail){
        this.name = name;
        this.surName = surName;
        this.mail = mail;
    }

    public String getName() {
        return name;
    }

    public String getSurName() {
        return surName;
    }

    public String getMail() {
        return mail;
    }
}
