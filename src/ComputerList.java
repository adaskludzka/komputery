import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ComputerList implements Serializable
{

    private List<Computer> computers;

    public ComputerList()
    {
        this.computers = new ArrayList<>();
    }

    public void addComputers(Computer computer)
    {
        this.computers.add(computer);
    }

    public List<Computer> getComputers()
    {
        return this.computers;
    }

    public void save(String name)
    {
        try (FileOutputStream fileOut = new FileOutputStream(name);
             ObjectOutputStream out = new ObjectOutputStream(fileOut))
        {
            out.writeObject(computers);

            System.out.println("Serializacja zakończona pomyślnie");
        } catch (Exception e)
        {
            System.out.println("Błąd podczas serializacji: " + e.getMessage());
        }

    }

    public void read(String name)
    {
        try (
                FileInputStream fileIn = new FileInputStream(name);
                ObjectInputStream in = new ObjectInputStream(fileIn))
        {
            this.computers = (List<Computer>) in.readObject();
        }
        catch (Exception e)
        {
            System.out.println("Błąd podczas deserializacji: " + e.getMessage());
        }
    }
}
