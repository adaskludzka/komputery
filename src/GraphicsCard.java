import java.io.Serializable;

public class GraphicsCard implements Serializable {

    private String type;
    private String name;

    public GraphicsCard(String type, String name){
        this.type = type;
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }
}
