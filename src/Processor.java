import java.io.Serializable;

public class Processor implements Serializable {

    private int clockSpeed;
    private String producer;
    private int numberOfCores;

    public Processor(int clockSpeed, String producer, int numberOfCores){
        this.clockSpeed = clockSpeed;
        this.producer = producer;
        this.numberOfCores = numberOfCores;
    }


    public int getClockSpeed() {
        return clockSpeed;
    }

    public String getProducer() {
        return producer;
    }

    public int getNumberOfCores() {
        return numberOfCores;
    }
}
