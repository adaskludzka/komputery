import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main
{

    public static void main(String[] args)
    {
        

//        Processor pr1 = new Processor(4000, "Intel",4);
//        Processor pr2 = new Processor(6000, "AMD", 6);
//        Processor pr3 = new Processor(3000, "Qualcomm", 2);
//
//        GraphicsCard gc1 = new GraphicsCard("zintegrowana","NVIDIA GeForce RTX 3080");
//        GraphicsCard gc2 = new GraphicsCard("zintegrowana","AMD Radeon RX 6800 XT");
//        GraphicsCard gc3 = new GraphicsCard("zintegrowana","Gigabyte Radeon RX 6400");
//        GraphicsCard gc4 = new GraphicsCard("dedykowana","Biostar Radeon RX 550");
//
//        User u1 = new User("Andrzej", "Nowak","andrzej.n@gmail.com");
//        User u2 = new User("Anna", "Pawlak","a.pawlak@gmail.com");
//        User u3 = new User("Leon", "Grzybowski","leon.g@gmail.com");
//        User u4 = new User("Konrad", "Kowalski","k.kowalski@gmail.com");
//        User u5 = new User("Wiola", "Wujek","wiola.w@gmail.com");
//
//        Computer comp1 = new Computer("Lenovo", "Legion", pr1, gc1, u1);
//        Computer comp2 = new Computer("Dell", "G15", pr1, gc2, u2);
//        Computer comp3 = new Computer("Lenovo", "IdeaPad", pr2, gc3, u3);
//        Computer comp4 = new Computer("HP", "Victus", pr3, gc4, u4);
//        Computer comp5 = new Computer("Acer", "Nitro", pr2, gc1, u5);


        ComputerList list = new ComputerList();

        String name = "out.ser";
        list.read(name);
//        list.addComputers(comp1);
//        list.addComputers(comp2);
//        list.addComputers(comp3);
//        list.addComputers(comp4);
//        list.addComputers(comp5);

        write(list.getComputers());

        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj nazwe procesora?");
        String proc = sc.next();
        System.out.println("Komputery z podanym procesorem:");
        write(list.getComputers().stream()
                .filter(k -> k.getProcessor().getProducer().toUpperCase().equals(proc.toUpperCase()))
                .collect(Collectors.toList()));

        Optional<Integer> max = list.getComputers().stream().map(k -> k.getProcessor().getClockSpeed()).max(Integer::compareTo);
        System.out.println();
        System.out.println("Maksymalne taktowanie procesora to: " + max.orElseThrow());
        System.out.println();
        list.save(name);

    }

    private static void write(List<Computer> list)
    {
        list.stream().forEach(computer ->
        {
            Processor processor = computer.getProcessor();
            GraphicsCard graphicsCard = computer.getGraphicsCard();
            User user = computer.getUser();

            
            System.out.println("Procesor: " + processor.getProducer() + ", rdzenie: " + processor.getNumberOfCores() + ", taktowanie: " + processor.getClockSpeed());
            System.out.println("Karta graficzna: " + graphicsCard.getName() + ", typ: " + graphicsCard.getType());
            System.out.println("Uzytkownik: " + user.getName() + user.getSurName() + ", mail: " + user.getMail());
//            
        });
    }
}
