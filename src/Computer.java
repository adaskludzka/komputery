import java.io.Serializable;

public class Computer implements Serializable {

    private String brand;
    private String model;
    private Processor processor;
    private GraphicsCard graphicsCard;
    private User user;


    public Computer(String brand, String model, Processor processor, GraphicsCard graphicsCard, User user){
        this.brand = brand;
        this.model = model;
        this.processor = processor;
        this.graphicsCard = graphicsCard;
        this.user = user;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public Processor getProcessor() {
        return processor;
    }

    public GraphicsCard getGraphicsCard() {
        return graphicsCard;
    }

    public User getUser() {
        return user;
    }
}

